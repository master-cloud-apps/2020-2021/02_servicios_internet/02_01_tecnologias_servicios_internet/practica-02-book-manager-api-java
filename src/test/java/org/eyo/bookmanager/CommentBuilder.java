package org.eyo.bookmanager;

import org.eyo.bookmanager.models.Comment;

public class CommentBuilder {

    private String content;
    private String name;
    private Float punctuation;
    private Long id;

    public CommentBuilder setContent(String content){
        this.content = content;
        return this;
    }

    public CommentBuilder setName(String name){
        this.name = name;
        return this;
    }

    public CommentBuilder setPunctuation(Float punctuation){
        this.punctuation = punctuation;
        return this;
    }

    public CommentBuilder setId(Long id){
        this.id = id;
        return this;
    }

    public Comment buildComment(){
        Comment newComment = new Comment();
        newComment.setId(this.id);
        newComment.setContent(this.content);
        newComment.setName(this.name);
        newComment.setPunctuation(this.punctuation);
        return newComment;
    }

    public String build(){
        return "{" +
                "\"content\": \""+ this.content+"\"," +
                "\"name\": \""+ this.name+"\"," +
                "\"punctuation\": "+ this.punctuation+"" +
                "}";
    }
}
