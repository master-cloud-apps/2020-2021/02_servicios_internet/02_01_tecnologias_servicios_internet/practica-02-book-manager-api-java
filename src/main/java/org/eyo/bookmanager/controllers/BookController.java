package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.api.BookAPI;
import org.eyo.bookmanager.dtos.requests.BookRequestDTO;
import org.eyo.bookmanager.dtos.responses.BookResponseDTO;
import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.services.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/books")
public class BookController implements BookAPI {

    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("")
    public ResponseEntity<Void> createBook(@RequestBody BookRequestDTO newBook) {
        Book bookToCreate = new Book();
        bookToCreate.setAuthor(newBook.getAuthor());
        bookToCreate.setEditorial(newBook.getEditorial());
        bookToCreate.setReview(newBook.getReview());
        bookToCreate.setTitle(newBook.getTitle());
        bookToCreate.setYearPublication(newBook.getYearPublication());
        this.bookService.save(bookToCreate);

        URI location = fromCurrentRequest().path("/{id}").buildAndExpand(bookToCreate.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("")
    public ResponseEntity<Collection<BookResponseDTO>> getBooks() {
        return ResponseEntity.ok(this.bookService.getAll()
                .stream()
                .map(BookResponseDTO::new)
                .collect(Collectors.toList()));
    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity<Void> deleteBookById(@PathVariable Long bookId) {
        Book bookToDelete = this.bookService.findById(bookId);

        if (bookToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        this.bookService.deleteById(bookId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<BookResponseDTO> getBookById(@PathVariable Long bookId) {
        if (this.bookService.findById(bookId) == null) {
            return ResponseEntity.notFound().build();
        }
        BookResponseDTO book = new BookResponseDTO(this.bookService.findById(bookId));
        return ResponseEntity.ok(book);
    }
}
