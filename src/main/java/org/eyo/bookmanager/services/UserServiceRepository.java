package org.eyo.bookmanager.services;

import org.eyo.bookmanager.models.User;
import org.eyo.bookmanager.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceRepository implements UserService {

    private UserRepository userRepository;

    public UserServiceRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void save(User user) {
        this.userRepository.save(user);
    }

    @Override
    public Collection<User> getAll() {
        return this.userRepository.getAll();
    }

    @Override
    public User findById(Long userId) {
        return this.userRepository.findById(userId);
    }

    @Override
    public void deleteById(Long userId) {
        this.userRepository.deleteById(userId);
    }

    @Override
    public User getUserByNick(String nick) {
        return this.userRepository.getUserByNick(nick);
    }
}
