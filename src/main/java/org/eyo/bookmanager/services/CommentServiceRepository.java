package org.eyo.bookmanager.services;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.repositories.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Service
public class CommentServiceRepository implements CommentService {

    private BookService bookService;

    private CommentRepository commentRepository;

    public CommentServiceRepository(BookService bookService, CommentRepository commentRepository) {
        this.bookService = bookService;
        this.commentRepository = commentRepository;
    }

    @Override
    public Collection<Comment> getAll(Long bookId) {
        Optional<Book> bookWithComments = this.bookService.getAll().stream()
                .filter(book -> book.getId().equals(bookId))
                .findAny();
        if (bookWithComments.isPresent()) {
            return bookWithComments.get().getComments();
        }
        return Collections.emptyList();
    }

    @Override
    public void save(Long bookId, Comment comment) {
        this.bookService.addCommentToBook(bookId, comment);
        this.commentRepository.save(bookId, comment);
    }

    @Override
    public void deleteComment(Long bookId, Long commentId) {
        this.commentRepository.deleteById(commentId);
        Book bookFromComment = this.bookService.findById(bookId);
        Comment commentToDelete = bookFromComment.getComments()
                .stream()
                .filter(c -> c.getId().equals(commentId))
                .findFirst().orElse(null);
        if (commentToDelete != null){
            bookFromComment.getComments().remove(commentToDelete);
            this.bookService.save(bookFromComment);
        }
    }

    @Override
    public Comment finById(Long commentId) {
        return this.commentRepository.finById(commentId);
    }
}
