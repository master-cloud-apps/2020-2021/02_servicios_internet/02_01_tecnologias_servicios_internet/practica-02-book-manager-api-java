package org.eyo.bookmanager.services;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.repositories.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class BookServiceRepository implements BookService {

    private BookRepository bookRepository;

    public BookServiceRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book save(Book book) {
        return this.bookRepository.save(book);
    }

    @Override
    public Collection<Book> getAll() {
        return this.bookRepository.getAll();
    }

    @Override
    public Book findById(Long bookId) {
        return this.bookRepository.findById(bookId);
    }

    @Override
    public void deleteById(Long bookId) {
        this.bookRepository.deleteById(bookId);
    }

    @Override
    public void addCommentToBook(Long bookId, Comment comment) {
        this.bookRepository.addCommentToBook(bookId, comment);
    }
}
