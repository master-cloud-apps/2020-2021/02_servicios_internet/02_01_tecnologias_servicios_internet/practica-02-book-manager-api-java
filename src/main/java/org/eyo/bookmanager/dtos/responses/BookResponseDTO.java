package org.eyo.bookmanager.dtos.responses;

import lombok.Getter;
import lombok.Setter;
import org.eyo.bookmanager.models.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class BookResponseDTO {

    public BookResponseDTO(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.editorial = book.getEditorial();
        this.yearPublication = book.getYearPublication();
        this.review = book.getReview();
        this.comments = book.getComments().stream().map(CommentBookResponseDTO::new).collect(Collectors.toList());
    }

    private Long id;
    private String title;
    private String review;
    private String author;
    private String editorial;
    private Long yearPublication;
    private List<CommentBookResponseDTO> comments = new ArrayList<>();
}
